import options, typetraits

type
    ЧётоЕсть = ref | ptr | pointer | proc

type
  `А?`*[T] = object
    when T is ЧётоЕсть:
      val: T
    else:
      val: T
      has: bool

proc Чё*[T](val: T): `А?`[T] {.inline.} = 
  when T is ЧётоЕсть :
    assert(not val.isNil)
    result.val = val
  else:
    result.has = true
    result.val = val

proc ЕстьЧё*[T](self: `А?`[T]): bool {.inline.} =
  when T is ЧётоЕсть:
    not self.val.isNil
  else:
    self.has

proc Ничё*[T](Чё: T): `А?`[T] {.inline.} = discard

proc ЕстьНичё*[T](self: `А?`[T]): bool {.inline.} =
  when T is ЧётоЕсть:
    self.val.isNil
  else:
    not self.has

proc sas(a: `А?`) = 
  if a.ЕстьЧё:
    echo a.Чё

sas Чё(4)  
sas Чё(nil)  

proc add1(x: int): int =
  x + 1

discard add1(1)
discard add1(x = 1)
discard 1.add1()
discard 1.add1

echo "hello world"

