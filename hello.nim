import locks
template withLock(lock: var Lock, body: typed) =
  acquire lock
  try:
    body
  finally:
    release lock


    
var someLock: Lock
initLock someLock

withLock someLock:
  echo "test"