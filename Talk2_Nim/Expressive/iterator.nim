echo "Counting to ten: "
for i in countup(1, 10):
  echo i

proc countup(a, b: int): int =
  var res = a
  while res <= b:
    return res # 
    inc(res)