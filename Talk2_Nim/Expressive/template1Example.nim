import macros, locks
template withLock(lock: var Lock, body: untyped) =
  acquire lock
  try:
    body
  finally:
    release lock


expandMacros:
  var someLock: Lock
  initLock someLock

  withLock someLock:
    echo "test"
