# https://nim-lang.github.io/Nim/enumerate.html
import std/enumerate
# show with macro exposed

let a = [10, 20, 30]
var b: seq[(int, int)]
for i, x in enumerate(a):
  b.add((i, x))
assert b == @[(0, 10), (1, 20), (2, 30)]

let c = "abcd"
var d: seq[(int, char)]
for (i, x) in enumerate(97, c):
  d.add((i, x))
assert d == @[(97, 'a'), (98, 'b'), (99, 'c'), (100, 'd')]
