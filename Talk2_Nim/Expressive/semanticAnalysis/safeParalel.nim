# Compute PI in an inefficient way
import strutils, math, threadpool
{.experimental: "parallel".}

proc term(k: float): float = 4 * math.pow(-1, k) / (2*k + 1)

proc pi(n: int): float =
  var ch = newSeq[float](n+1)
  parallel:
    for k in 0..ch.high:
      ch[k] = spawn term(float(k))
  for k in 0..ch.high:
    result += ch[k]

echo formatFloat(pi(5000))


################ error

var a = @[1, 3, 4, 5]

proc f(q: int) =
  discard

parallel:
  var i = 0
  while i <= a.high:
    spawn f(a[i])
    spawn f(a[i + 1])
    i += 1
        