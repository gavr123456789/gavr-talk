{.experimental: "strictFuncs".}

type
  Node = ref object
    le, ri: Node
    data: string

func len(n: Node): int =
  # valid: len does not have side effects
  var it = n
  while it != nil:
    inc result
    it = it.ri

func mut(n: Node) =
  let m = n # is the statement that connected the mutation to the parameter
  m.data = "yeah" 


# more complicated
# func mut2(n: Node): Node = 
#   let x = n
#   let y = x
#   return y

# func mut(n: Node) =
#   let m = n
#   let q = mut2(m)
#   q.data = "8"