type
  Expression = ref object of RootObj ## abstract base class for an expression
  Literal = ref object of Expression
    x: int
  PlusExpr = ref object of Expression
    a, b: Expression

method eval(e: Expression): int {.base.} =
  echo "eval Expression"

  # override this base method
  raise newException(CatchableError, "Method without implementation override")

method eval(e: Literal): int = 
  echo "eval Literal"
  return e.x

method eval(e: PlusExpr): int =
  echo "eval PlusExpr"

  # watch out: relies on dynamic binding
  result = eval(e.a) + eval(e.b)

proc newLit(x: int): Literal =
  echo "newLit ", x
  new(result)
  result.x = x

proc newPlus(a, b: Expression): PlusExpr =
  echo "newPlus 2 Expression"

  new(result)
  result.a = a
  result.b = b

echo eval(newPlus(newPlus(newLit(1), newLit(2)), newLit(4)))