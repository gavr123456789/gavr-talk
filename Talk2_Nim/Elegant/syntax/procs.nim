# procs
proc add1(x: int, y: int): int =
  return x + y


proc add2(x, y: int): int =
  result = x + y
  


proc add3(x, y: int) =
  echo x + y

# discard add1(1, 2)
# add1 1, 2
# 1.add1(2)
# 1.add1 2

# var qwe = 43

# funcs
func add4(x, y: int): int = 
  # qwe += 23
  debugEcho "sas"
  x + y
