
proc getAlphabet(): string =
  var accm = ""
  for letter in 'a'..'z':  # see iterators
    accm.add(letter)
  echo accm
  return accm


# var
# var
#   a = "foo"
#   b = 0
#   # Works fine, initialized to 0
#   c: int
# # let
# let
#   d = "foo"
#   e = 5
#   # Compile-time error, must be initialized at creation
#   f: float

  
# const
const alphabet = getAlphabet()


# Mutable variables


# Immutable variables
