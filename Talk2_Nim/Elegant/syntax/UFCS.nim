import math

proc findGCD(nums: seq[int]): int =
  # gcd(nums.min, nums.max)
  nums.min.gcd nums.max
