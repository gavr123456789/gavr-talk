class Klass {
  public int x;
}

void qwe(owned Klass klass){
  klass.x = 42;
}

void main(){
  var x = new Klass();
  qwe(x);
}
