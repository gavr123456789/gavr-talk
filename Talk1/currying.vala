delegate int intOp(int d);

intOp plus(int a) {
  return b => a + b;
}

void main() {
  var a = plus(3)(4);
  prin(a);
  var b = plus(2) (plus(25)(15));
  prin(b);
}


[Print]
void prin(string s) {stdout.printf(s + "\n");}