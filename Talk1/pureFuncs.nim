
var someGlobalData = 23

# вызов грфзкой функции из чистой
proc qwe =
  someGlobalData = 25

# изменение глобальной переменной
func pure(age: int, name: string): string = 
  name & ": " & $age


echo pure(age = 100, name = "qwe")

qwe()