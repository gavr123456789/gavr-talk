// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp

open System

type CreditCardPaymentMethod = 
    | OnSite
    | Courier

type PaymentMethod = 
    | Cash
    | CreditCard of CreditCardPaymentMethod
    | WebMoney of string

let payWithWebMoney acc amount =
    printfn "fgh"

// Использование
let webMoney = WebMoney "123456"
let cash = Cash
let creditCardCourier = CreditCard Courier
// Обработка
let processPayment paymentMethod amount = 
    match paymentMethod with
    | Cash | CreditCard Courier -> async.Return(Ok())
    | WebMoney acc -> payWithWebMoney acc amount
    // | WebMoney acc -> async.Return(Error("dfg"))
    | CreditCard (OnSide) -> printfn "qwe"

// Define a function to construct a message to print
let from whom =
    sprintf "from %s" whom

[<EntryPoint>]
let main argv =
    let message = from "F#" // Call the function
    printfn "Hello world %s" message
    0 // return an integer exit code