enum T {
  Courier,
  Cash
}

class CreditCardInfo {
  id: number
  name: string
}

type CreditCardPaymentMethod = 
  | CreditCardInfo
  | T.Courier

type WebMoney = string

type PaymentMethod =
  | T.Cash 
  | CreditCardInfo 
  | WebMoney


// Использование

const webMoney: PaymentMethod = "123456789"
const cash: PaymentMethod = T.Cash
const creditCardOnSite: PaymentMethod = {id: 54, name: "Ivan"}



type NetworkLoadingState = {
  state: "loading";
};
type NetworkFailedState = {
  state: "failed";
  code: number;
};
type NetworkSuccessState = {
  state: "success";
  response: {
    title: string;
    duration: number;
    summary: string;
  };
};
// Create a type which represents only one of the above types
// but you aren't sure which it is yet.
type NetworkState =
  | NetworkLoadingState
  | NetworkFailedState
  | NetworkSuccessState;
