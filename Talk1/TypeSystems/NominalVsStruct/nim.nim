
type
  PersonStruct = tuple
    name: string
    age: int

  PersonNominal = ref object
    name: string
    age: int

var a1: PersonStruct = (name: "213", age: 34)
var a2: PersonStruct = (name: "213", age: 34)

echo a1 == a2

var b1 = PersonNominal(name: "213", age: 34)
var b2 = PersonNominal(age: 34, name: "213")

echo b1 == b2
