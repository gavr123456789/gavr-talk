

interface Person{
  readonly name: string;
  readonly age: number
}

const person: Person = {
  age: 20,
  name: 'Ivan'
}

// person.age = 23
// const oldperson = {...person, age: 100}


interface Person2{
  name: string;
  age: number
}

type ImmutalPerson = Readonly<Person2>

const igor: ImmutalPerson = {age: 42, name: 'qwe'}
igor.age = 321
