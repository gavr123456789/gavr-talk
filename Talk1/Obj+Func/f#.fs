type Student ()=   
 class  
   let mutable id = 0  
   let mutable name = " "  
       
   member x.Insert(v1, v2) =   
        id <- v1  
        name <-v2   
   member x.Show = printfn "%d %s" id name  
  
  
 end  
let a = new Student()  
a.Insert(25,"RajKumar")  
a.Show    
a.Insert(26,"John")  
a.Show  
a.Insert(27,"Mariam")  
a.Show  


// 25 RajKumar
// 26 John
// 27 Mariam